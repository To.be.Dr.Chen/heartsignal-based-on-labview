Through this interface based on Labview 2018, we can obtain ECG/PLT signals from the human body with Einthoven II lead. However, it can also be adaptive to other leads with slight modification. 

Signals can be tested with a Daq card mixed with many noises. So firstly, several filters have been set to improve the SNR. Wavelet transform unit could also be used, but only available after version. Eventually, after processing with the algorithms in folder Other_Sub_VI, information like heartrate(bpm), FFT, R-Zacke, QRS-waves can be shown in the interface.

There are two entries for this project. 
File "Biopropcessor.VI" is the access to real-time testing.
File "Bioprocessor_Read.VI" is the access to invert raw data into beautiful ECG/PPT charts.

